#Zadanie rekrutacyjne Bee2Code - Michał Grochowski
Część front-endowa strony

Aby uruchomić i móc pracować nad częścią back-endową należy:
1.Pobrać Visual Studio Coode
2.Pobrać NodeJS ze strony https://nodejs.org/
3.Zainstalować NodeJS, albo przez instalator, ablo przez menadżer w VS Code (ctrl+shift+` i wpisać komendę nvm install -g npm)
4.Pliki strony umieścić w folderze na swoim komputerze i otworzyć ten folder w VS Code
5.Pobrać dodatkowe biblioteki użyte podczas robienia strony komendą: npm install --save chart.js react-chartjs-2
6.Wpisać komendę npm start
7.Można pracować