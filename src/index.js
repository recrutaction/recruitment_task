import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/index.css';
import './css/menu.css';
import './css/summary.css';
import './css/patients.css';
import './css/projects.css';
import './css/orders.css';
import './css/results.css';
import './css/addEdit.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)

reportWebVitals();
