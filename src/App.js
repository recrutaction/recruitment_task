import Menu from './components/Menu'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Summary from './components/summary/Summary'
import Patients from './components/patients/Patients';
import Projects from './components/projects/Projects';
import Orders from './components/orders/Orders';
import Results from './components/results/Results';

import AddPatient from './components/patients/AddPatient';
import AddProject from './components/projects/AddProject';
import AddOrder from './components/orders/AddOrder';
import AddTest from './components/results/AddTest';

import EditPatient from './components/patients/EditPatient';
import EditProject from './components/projects/EditProject';
import EditTest from './components/results/EditTest';

import OrderInfo from './components/orders/OrderInfo';

import EditParticipants from './components/projects/EditParticipants';
import OrderInfoEdit from './components/orders/OrderInfoEdit';

function App() {

  return (
    <Router>
      <div className="container">
        <Menu />
        <Routes>
          <Route path="/" element={<Summary />} />
          <Route path="/patients" element={<Patients />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/results" element={<Results />} />
          
          <Route path="/patients/addPatient" element={<AddPatient />} />
          <Route path="/projects/addProject" element={<AddProject />} />
          <Route path="/orders/addOrder" element={<AddOrder />} />
          <Route path="/results/addTest" element={<AddTest />} />

          <Route path="/patients/editPatient" element={<EditPatient />} />
          <Route path="/projects/editProject" element={<EditProject />} />
          <Route path="/results/editTest" element={<EditTest />} />

          <Route path="/orders/orderInfo" element={<OrderInfo />} />
          <Route path="/projects/editParticipants" element={<EditParticipants />} />
          <Route path="/orders/orderInfoEdit" element={<OrderInfoEdit />} />
        </Routes>
      </div>
    </Router>
  )

}

export default App;
