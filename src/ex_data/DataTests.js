export const DataTests = [
   {
      id: 1,
      testName: 'Test1',
      result: "Negatywny",
      orderId: 5,
      patId: 1,
      executed: 54
   },
   {
      id: 2,
      testName: 'Test2',
      result: "-",
      orderId: 1,
      patId: 3,
      executed: 14
   },
   {
      id: 3,
      testName: 'Test3',
      result: "Pozytywny",
      orderId: 3,
      patId: 14,
      executed: 42
   },
   {
      id: 4,
      testName: 'Test4',
      result: "Pozytywny",
      orderId: 1,
      patId: 16,
      executed: 24
   },
   {
      id: 5,
      testName: 'Test5',
      result: "Pozytywny",
      orderId: 1,
      patId: 16,
      executed: 12
   },
   {
      id: 6,
      testName: 'Test6',
      result: "-",
      orderId: 1,
      patId: 10,
      executed: 32
   },
   {
      id: 7,
      testName: 'Test7',
      result: "-",
      orderId: 1,
      patId: 3,
      executed: 7
   },
   {
      id: 8,
      testName: 'Test8',
      result: "Negatywny",
      orderId: 1,
      patId: 10,
      executed: 15
   },
   {
      id: 9,
      testName: 'Test9',
      result: "Pozytywny",
      orderId: 1,
      patId: 4,
      executed: 27
   },
   {
      id: 10,
      testName: 'Test10',
      result: "Negatywny",
      orderId: 1,
      patId: 3,
      executed: 12
   }
]