export const DataProjects = [
   {
      id: 1,
      projName: 'Proj 1',
      numbOfPat: 1,
      patId: 1,
   },
   {
      id: 2,
      projName: 'Proj 2',
      numbOfPat: 2,
      patId: 5
   },
   {
      id: 3,
      projName: 'Proj 3',
      numbOfPat: 3,
      patId: 3
   },
   {
      id: 4,
      projName: 'Proj 4',
      numbOfPat: 4,
      patId: 4
   },
   {
      id: 5,
      projName: 'Proj 5',
      numbOfPat: 5,
      patId: 1
   },
   {
      id: 6,
      projName: 'Proj 6',
      numbOfPat: 6,
      patId: 2
   },
   {
      id: 7,
      projName: 'Proj 7',
      numbOfPat: 7,
      patId: 7
   },
   {
      id: 8,
      projName: 'Proj 8',
      numbOfPat: 8,
      patId: 37
   },
   {
      id: 9,
      projName: 'Proj 9',
      numbOfPat: 9,
      patId: 24
   },
   {
      id: 10,
      projName: 'Proj 10',
      numbOfPat: 10,
      patId: 10
   },
   {
      id: 11,
      projName: 'Proj 11',
      numbOfPat: 11,
      patId: 5
   },
   {
      id: 12,
      projName: 'Proj 12',
      numbOfPat: 12,
      patId: 2
   },
   {
      id: 13,
      projName: 'Proj 13',
      numbOfPat: 13,
      patId: 3
   },
   {
      id: 14,
      projName: 'Proj 14',
      numbOfPat: 14,
      patId: 1
   },
   {
      id: 15,
      projName: 'Proj 15',
      numbOfPat: 15,
      patId: 14
   },
   {
      id: 16,
      projName: 'Proj 16',
      numbOfPat: 16,
      patId: 63
   },
   {
      id: 17,
      projName: 'Proj 17',
      numbOfPat: 17,
      patId: 15
   },
   {
      id: 18,
      projName: 'Proj 18',
      numbOfPat: 18,
      patId: 2
   },
   {
      id: 19,
      projName: 'Proj 19',
      numbOfPat: 19,
      patId: 1
   },
   {
      id: 20,
      projName: 'Proj 20',
      numbOfPat: 20,
      patId: 16
   },
   {
      id: 21,
      projName: 'Proj 21',
      numbOfPat: 21,
      patId: 5
   },
]