import React from 'react'
import { Link } from "react-router-dom"
import ProjectsList from "./ProjectsList"

function Projects() {

  return (
    <div className='projects-tab'>
      <div className="top">
         <div className="search">
            <input type="text" placeholder="Wyszukaj projekt"/>
         </div>
         <div className="add">
            <Link to="/projects/addProject" className='add-project'>Dodaj projekt</Link>
         </div>
      </div>
         <ProjectsList /> 
    </div>
  )
}

export default Projects
