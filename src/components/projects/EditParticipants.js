import React from 'react'
import { Link } from "react-router-dom";
import { DataPatients } from '../../ex_data/DataPatients'
import ShowEditParticipants from './ShowEditParticipants'


function EditParticipants() {
   const checkIfCorrect = () => {
      let correct = true
      const patNameCheck = document.getElementById("patNameCheck");

      if(patNameCheck.value === ""){
         patNameCheck.classList.add('listError')
         correct = false
      }
      
      if(patNameCheck.value !== ""){
         patNameCheck.classList.remove('listError')
      }

      if(correct){
         console.log("Add to database:\n\tProject name: "+patNameCheck.value)
         window.location.href = "/projects";
      }
   }

   const queryString = window.location.search;
   const urlParam = new URLSearchParams(queryString);
   const project_id = urlParam.get('project_id')

  return (
    <div>
      <div className="box">
         <div className="main">
            <h1>Edytuj uczestników projektu</h1>
            <form action='###'>
               <div className="entering dataEditing">
                  <ShowEditParticipants projID={project_id} />
               </div>
               <select name='patName' id ="patNameCheck" className='edit-pat-in-proj' defaultValue="">
                  <option value="" disabled>Wybierz pacjenta</option>
                  {DataPatients.map((data) =>
                     <option value={"patId_"+data.id} key={"key_"+data.id} className={data.agreed}>
                        {data.nameS} {data.surname}, {data.pesel}, {data.city} {data.address}
                     </option>
                  )}      
               </select>
                  <div className="legend">
                     <div className="green">
                        <p>Zgoda na badania</p>
                     </div>
                     <div className="red">
                        <p>Brak zgody na badania</p>
                     </div>
                  </div>
                  <div className="addOption">
                     <h2>Dodaj</h2>
                  </div>
               <div className="buttons">
                  <Link to="/projects" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Zapisz" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default EditParticipants
