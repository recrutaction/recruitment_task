import React from 'react'
import { DataPatients } from '../../ex_data/DataPatients'

function ShowParticipants(gottenProjectId) {
   const chosenProject = gottenProjectId.projId
   return (
      <>
      {DataPatients.map((data) =>
         data.projId === parseInt(chosenProject) &&
         <tr id={"projId_"+data.projId} className="details" key={"key_"+data.id}>
            <td>
               {data.nameS} {data.surname}, {data.pesel}, {data.city} {data.address}
            </td>
            <td>
               {data.agreed ? "TAK": "NIE"}
            </td>
         </tr>
      )}
      </>
   )
}

export default ShowParticipants
