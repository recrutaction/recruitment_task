import React from 'react'
import { DataPatients } from '../../ex_data/DataPatients'

function ShowEditParticipants(gottenProjId) {
   const projectIdFromUrl = gottenProjId.projID
   return (
      <>
      {DataPatients.map((data) =>
         data.projId === parseInt(projectIdFromUrl) &&
         <div id={"id_"+data.projId} className="in-project-patient" key={"key_"+data.id}>
            <div className="del" onClick={() => document.getElementById("id_"+data.projId).remove()}>
               <p>X</p>
            </div>
            <div className="patChosenName">
               <input id="input_`+sel+`"type="text" className="chosen-patient" disabled value={data.nameS+", "+data.surname+", "+data.pesel+", "+data.city+", "+data.address}/>
               <p>{data.nameS}, {data.surname}, {data.pesel}, {data.city}, {data.address}</p>
            </div>
            <div className='agreedCheckBox'>
               <p>Zgoda</p>
               <input id="check_`+sel+`"className='agreed' type="checkbox" defaultChecked={data.agreed}/>
            </div>
         </div>
      )}
      </>
   )
}

export default ShowEditParticipants
