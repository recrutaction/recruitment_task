import React from 'react'
import { Link } from "react-router-dom";


function AddProject() {
   const checkIfCorrect = () => {
      let correct = true
      const nameCheck = document.getElementById("nameCheck");

      if(!nameCheck.value){
         nameCheck.classList.add('error')
         correct = false
      }
      
      if(nameCheck.value !== ""){
         nameCheck.classList.remove('error')
      }

      if(correct){
         console.log("Add to database:\n\tProject name: "+nameCheck.value)
         window.location.href = "/projects";
      }
   }

  return (
    <div>
      <div className="box">
         <div className="main">
            <h1>Utwórz projekt</h1>
            <form action='###'>
               <div className="entering">
                  <label htmlFor="projName"><r>*</r>Nazwa</label>
                  <input id ="nameCheck" type="text" name='projName' placeholder='Wprowadź nazwę projektu' required/>
               </div>
               <div className="buttons">
                  <Link to="/projects" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Utwórz" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default AddProject
