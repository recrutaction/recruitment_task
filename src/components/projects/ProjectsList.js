import React from 'react'
import { DataProjects } from "../../ex_data/DataProjects";
import { Link } from "react-router-dom";
import DotsProj from './DotsProj'
import { useState } from 'react'
import arrow from '../../img/arrow.png'
import ShowParticipants from './ShowParticipants';

function ProjectList() {
   const [ChosenProject, setChosenProject] = useState({
      id: "",
      projName: "",
      numbOfPat: "",
   })
 
   const showDetails = (projId) => {
      const idOfProj = projId.split("_").pop()
      DataProjects.map((data, index) =>
         index === parseInt(idOfProj)-1 &&
         setChosenProject({
            id: idOfProj,
            projName: data.projName,
            numbOfPat: data.numbOfPat
         })
      )
      document.querySelector('.project-details').style.display = "flex"
   }

   var sortedAZ = true
   var currentSorted = "projId"
   const sortProj = (sortBy) => {
      const idArrow1 = document.getElementById("arrow-for-id-1")
      const idArrow2 = document.getElementById("arrow-for-id-2")
      const nameArrow1 = document.getElementById("arrow-for-proj-name-1")
      const nameArrow2 = document.getElementById("arrow-for-proj-name-2")
      const numbOfPatArrow1 = document.getElementById("arrow-for-num-of-pat-1")
      const numbOfPatArrow2 = document.getElementById("arrow-for-num-of-pat-2")
      const arrowCurrent1 = document.querySelector(".active-1")
      const arrowCurrent2 = document.querySelector(".active-2")

      switch (sortBy) {
         case "projId":
            if(currentSorted !== "projId"){
               sortedAZ = true
               currentSorted = "projId"
               console.log("Column ID sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               idArrow1.classList.add("active-1")
               idArrow2.classList.add("active-2")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else if(currentSorted === "projId" && !sortedAZ){
               sortedAZ = true
               currentSorted = "projId"
               console.log("Column ID sorted ascendingly-2")
               
               idArrow1.classList.remove("desc")
               idArrow2.classList.remove("desc")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "projId"
               console.log("Column ID sorted descendingly-3")

               idArrow1.classList.remove("asc")
               idArrow2.classList.remove("asc")
               idArrow2.classList.add("desc")
               idArrow1.classList.add("desc")
            }
            break;
         case "projName":
            if(currentSorted !== "projName"){
               sortedAZ = true
               currentSorted = "projName"
               console.log("Column project name sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               nameArrow1.classList.add("active-1")
               nameArrow2.classList.add("active-2")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else if(currentSorted === "projName" && !sortedAZ){
               sortedAZ = true
               currentSorted = "projName"
               console.log("Column project name sorted ascendingly-2")
               
               nameArrow1.classList.remove("desc")
               nameArrow2.classList.remove("desc")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "projName"
               console.log("Column project name sorted descendingly-3")

               nameArrow1.classList.remove("asc")
               nameArrow2.classList.remove("asc")
               nameArrow1.classList.add("desc")
               nameArrow2.classList.add("desc")
            }
            break;
         case "numbOfPat":
            if(currentSorted !== "numbOfPat"){
               sortedAZ = true
               currentSorted = "numbOfPat"
               console.log("Column number of participants sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               numbOfPatArrow1.classList.add("active-1")
               numbOfPatArrow2.classList.add("active-2")
               numbOfPatArrow1.classList.add("asc")
               numbOfPatArrow2.classList.add("asc")
            }
            else if(currentSorted === "numbOfPat" && !sortedAZ){
               sortedAZ = true
               currentSorted = "numbOfPat"
               console.log("Column number of participants sorted ascendingly-2")
               
               numbOfPatArrow1.classList.remove("desc")
               numbOfPatArrow2.classList.remove("desc")
               numbOfPatArrow1.classList.add("asc")
               numbOfPatArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "numbOfPat"
               console.log("Column number of participants sorted descendingly-3")

               numbOfPatArrow1.classList.remove("asc")
               numbOfPatArrow2.classList.remove("asc")
               numbOfPatArrow1.classList.add("desc")
               numbOfPatArrow2.classList.add("desc")
            }
            break;
         default:
            console.log("ERROR")
            break;
      }
      
   }

   const hideExtra = () => document.querySelector('.project-details').style.display = "none"
   const [page, setPage] = useState(1);
   const minPageId = (page - 1) * 10
   const maxPageId = page * 10

   
  return (
    <>
      <div className="project-details">
         <div className="info">
            <h1>{ChosenProject.projName}</h1>
            <div className="line">
               <h2>Id</h2>
               <p>{ChosenProject.id}</p>
            </div>
            <div className="line">
               <h2>Ilośc uczestników projektu</h2>
               <p>{ChosenProject.numbOfPat}</p>
            </div>
         </div>
         <div className="proj">
            <h1>Uczestnicy</h1>
            <div className="overflow">
               <table className="detail-info">
                  <thead>
                     <tr>
                        <th>Dane pacjenta</th>
                        <th>Zgoda na badania</th>
                     </tr>
                  </thead>
                  <tbody>
                     <ShowParticipants projId={ChosenProject.id}/>
                  </tbody>
               </table>
            </div>
         </div>
         <div className="settings">
            <div className="edit-delete">
               <div className="delete">
                  <p>Usuń</p>
               </div>
               <Link to={"/projects/editProject?"+ChosenProject.id} className="edit">
                  <p>Edytuj</p>
               </Link>
            </div>
            <div className="go-back">
               <Link to={"/projects/editParticipants?project_id="+ChosenProject.id} className="edit-participants">
                  <p>Edutuj uczestników</p>
               </Link>
               <div className="close" onClick={() => hideExtra()}>
                  <p>Zamknij</p>
               </div>
            </div>
         </div>
      </div>
      <table className="project-list">
         <tbody>
            <tr>
               <th onClick={() => sortProj("projId")} id='proj-id' className='id'>
                  <div id='sort-id' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-1" className='sorted asc active-1'/>
                     <h2>Id</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-2" className='sorted asc active-2'/>
                  </div>
               </th>
               <th onClick={() => sortProj("projName")} id='proj-name' className='proj-name'>
                  <div id='sort-proj-name' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-proj-name-1"className='sorted'/>
                     <h2>Nazwa Projektu</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-proj-name-2"className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortProj("numbOfPat")} id='num-of-pat' className='num-of-pat'>
                  <div id='sort-num-of-pat' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-num-of-pat-1" className='sorted'/>
                     <h2>Ilość Uczestników</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-num-of-pat-2" className='sorted'/>
                  </div>
               </th>
               <th className='manage'></th>
            </tr>
         {DataProjects.map((data, index) =>
         index >= minPageId && index < maxPageId &&
            <tr key={"proj_id_" + data.id}>
               <td className="id">{data.id}</td>
               <td className="projName">{data.projName}</td>
               <td className="numbOfPat">{data.numbOfPat}</td>
               <td className="info">
                  <div className="more" onClick={() => showDetails("proj_id_" + data.id)}>
                     <p>Więcej</p>
                  </div>
               </td> 
            </tr>
         )}
         </tbody>
      </table>
      <div className="bot">
            <DotsProj setPage={setPage} />
      </div>
         
    </>
  )
}

export default ProjectList
