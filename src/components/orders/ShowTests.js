import React from 'react'
import { DataTests } from '../../ex_data/DataTests'

function ShowTests(gottenOrderId) {
   const chosenTests = gottenOrderId.orderId
   return (
      <>
      {DataTests.map((data) =>
         data.orderId === parseInt(chosenTests) &&
         <tr id={"orderId"+data.orderId} className="details" key={"key_"+data.id}>
            <td>
               {data.testName}
            </td>
            <td className='small'>
               {data.result}
            </td>
         </tr>
      )}
      </>
   )
}

export default ShowTests
