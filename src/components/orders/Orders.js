import React from 'react'
import { Link } from "react-router-dom"
import OrdersList from "./OrdersList"

function Orders() {

  return (
    <div className='orders-tab'>
      <div className="top">
         <div className="search">
            <input type="text" placeholder="Wyszukaj zamówienie"/>
         </div>
         <div className="add">
            <Link to="/orders/addOrder" className='add-order'>Dodaj zamówienie</Link>
         </div>
      </div>
         <OrdersList /> 
    </div>
  )
}

export default Orders
