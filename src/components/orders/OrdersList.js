import React from 'react'
import { DataOrders } from "../../ex_data/DataOrders";
import { Link } from "react-router-dom";
import DotsOrder from './DotsOrder'
import { useState } from 'react'
import arrow from '../../img/arrow.png'
import ShowTests from './ShowTests';

function OrdersList() {
   const [ChosenOrder] = useState({
      id: "",
      orderName: "",
      date: "",
      numbOfPat: "",
   })

   var sortedAZ = true
   var currentSorted = "ordId"
   const sortOrd = (sortBy) => {
      const idArrow1 = document.getElementById("arrow-for-id-1")
      const idArrow2 = document.getElementById("arrow-for-id-2")
      const nameArrow1 = document.getElementById("arrow-for-ord-name-1")
      const nameArrow2 = document.getElementById("arrow-for-ord-name-2")
      const dateArrow1 = document.getElementById("arrow-for-date-1")
      const dateArrow2 = document.getElementById("arrow-for-date-2")
      const orderProjectArrow1 = document.getElementById("arrow-for-order-project-1")
      const orderProjectArrow2 = document.getElementById("arrow-for-order-project-2")
      const arrowCurrent1 = document.querySelector(".active-1")
      const arrowCurrent2 = document.querySelector(".active-2")

      switch (sortBy) {
         case "ordId":
            if(currentSorted !== "ordId"){
               sortedAZ = true
               currentSorted = "ordId"
               console.log("Column ID sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               idArrow1.classList.add("active-1")
               idArrow2.classList.add("active-2")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else if(currentSorted === "ordId" && !sortedAZ){
               sortedAZ = true
               currentSorted = "ordId"
               
               idArrow1.classList.remove("desc")
               idArrow2.classList.remove("desc")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "ordId"

               idArrow1.classList.remove("asc")
               idArrow2.classList.remove("asc")
               idArrow2.classList.add("desc")
               idArrow1.classList.add("desc")
            }
            break;
         case "ordName":
            if(currentSorted !== "ordName"){
               sortedAZ = true
               currentSorted = "ordName"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               nameArrow1.classList.add("active-1")
               nameArrow2.classList.add("active-2")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else if(currentSorted === "ordName" && !sortedAZ){
               sortedAZ = true
               currentSorted = "ordName"
               
               nameArrow1.classList.remove("desc")
               nameArrow2.classList.remove("desc")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "ordName"

               nameArrow1.classList.remove("asc")
               nameArrow2.classList.remove("asc")
               nameArrow1.classList.add("desc")
               nameArrow2.classList.add("desc")
            }
            break;
         case "date":
            if(currentSorted !== "date"){
               sortedAZ = true
               currentSorted = "date"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               dateArrow1.classList.add("active-1")
               dateArrow2.classList.add("active-2")
               dateArrow1.classList.add("asc")
               dateArrow2.classList.add("asc")
            }
            else if(currentSorted === "date" && !sortedAZ){
               sortedAZ = true
               currentSorted = "date"
               
               dateArrow1.classList.remove("desc")
               dateArrow2.classList.remove("desc")
               dateArrow1.classList.add("asc")
               dateArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "date"

               dateArrow1.classList.remove("asc")
               dateArrow2.classList.remove("asc")
               dateArrow1.classList.add("desc")
               dateArrow2.classList.add("desc")
            }
            break;
         case "orderProject":
            if(currentSorted !== "orderProject"){
               sortedAZ = true
               currentSorted = "orderProject"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               orderProjectArrow1.classList.add("active-1")
               orderProjectArrow2.classList.add("active-2")
               orderProjectArrow1.classList.add("asc")
               orderProjectArrow2.classList.add("asc")
            }
            else if(currentSorted === "orderProject" && !sortedAZ){
               sortedAZ = true
               currentSorted = "orderProject"
               
               orderProjectArrow1.classList.remove("desc")
               orderProjectArrow2.classList.remove("desc")
               orderProjectArrow1.classList.add("asc")
               orderProjectArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "orderProject"

               orderProjectArrow1.classList.remove("asc")
               orderProjectArrow2.classList.remove("asc")
               orderProjectArrow1.classList.add("desc")
               orderProjectArrow2.classList.add("desc")
            }
            break;
         default:
            console.log("ERROR")
            break;
      }
      
   }

   const hideExtra = () => document.querySelector('.order-details').style.display = "none"
   const [page, setPage] = useState(1);
   const minPageId = (page - 1) * 10
   const maxPageId = page * 10

   
  return (
    <>
      <div className="order-details">
         <div className="info-order">
            <h1>{ChosenOrder.orderName}</h1>
            <h2>{ChosenOrder.date}</h2>
            <div className="test">
               <div className="overflow">
                  <table className="detail-info">
                     <thead>
                        <tr>
                           <th>Zamówienia</th>
                           <th>Wynik</th>
                        </tr>
                     </thead>
                     <tbody>
                        {<ShowTests orderId={ChosenOrder.id}/>}
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div className="settings">
            <div className="edit-delete">
               <div className="delete">
                  <p>Usuń</p>
               </div>
               <Link to={"/orders/orderInfo?ord_id="+ChosenOrder.id} className="edit">
                  <p>Edytuj</p>
               </Link>
            </div>
            <div className="go-back">
               <div className="close" onClick={() => hideExtra()}>
                  <p>Zamknij</p>
               </div>
            </div>
         </div>
      </div>
      <table className="order-list">
         <tbody>
            <tr>
               <th onClick={() => sortOrd("ordId")} id='ord-id' className='id'>
                  <div id='sort-id' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-1" className='sorted asc active-1'/>
                     <h2>Id</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-2" className='sorted asc active-2'/>
                  </div>
               </th>
               <th onClick={() => sortOrd("ordName")} id='ord-name' className='ord-name'>
                  <div id='sort-ord-name' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-ord-name-1"className='sorted'/>
                     <h2>Nazwa Zlecenia</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-ord-name-2"className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortOrd("date")} id='date' className='date'>
                  <div id='sort-date' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-date-1" className='sorted'/>
                     <h2>Data Dodania</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-date-2" className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortOrd("orderProject")} id='order-project' className='order-project'>
                  <div id='sort-order-project' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-order-project-1" className='sorted'/>
                     <h2>Projekt</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-order-project-2" className='sorted'/>
                  </div>
               </th>
               <th className='manage'></th>
            </tr>
         {DataOrders.map((data, index) =>
         index >= minPageId && index < maxPageId &&
            <tr key={"ord_id_" + data.id}>
               <td className="id">{data.id}</td>
               <td className="ordName">{data.orderName}</td>
               <td className="date">{data.date}</td>
               <td className="orderProject">{data.projId}</td>
               <td className="info">
                  <Link to={"/orders/orderInfo?ord_id=" + data.id} className="more">
                     Więcej
                  </Link>
               </td> 
            </tr>
         )}
         </tbody>
      </table>
      <div className="bot">
            <DotsOrder setPage={setPage} />
      </div>
         
    </>
  )
}

export default OrdersList
