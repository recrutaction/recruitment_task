import React from 'react'
import { Link } from "react-router-dom";
import { DataOrders } from '../../ex_data/DataOrders';
import { DataPatients } from '../../ex_data/DataPatients';
import { DataTests } from '../../ex_data/DataTests';


function OrderInfo() {
   const queryString = window.location.search;
   const urlParam = new URLSearchParams(queryString);
   const order_id = urlParam.get('ord_id')

   const deleteOrder = () => {
      DataOrders.map((data) => {
         if (data.id === parseInt(order_id) && !data.done) console.log("Delete an order")
         else if(data.id === parseInt(order_id) && data.done) alert("Zlecenie zostało już zrealizowane")&&<></>
      })
   }

  return (
    <div>
      <div className="box">
         <div className="infoOrderMain">
            {DataOrders.map((data) => 
               data.id === parseInt(order_id) &&
               <div className='infoOrderBox' key={"key_4_"+data.id}>
                  <h1>{data.orderName}</h1>
                  <div className="infoOrderId infoOrderElement">
                     <h2>Id zamówienia:</h2>
                     <p>{data.id}</p>
                  </div>
                  <div className="infoOrderId infoOrderElement">
                     <h2>Zrealizowano:</h2>
                     <p>{data.done ? "TAK": "NIE"}</p>
                  </div>
                  <div className="infoOrderProject infoOrderElement">
                     <h2>Data utworzenia:</h2>
                     <p>{data.date}</p>
                  </div>
                  <div className="infoOrderDate infoOrderElement">
                     <h2>Projekt:</h2>
                     <p>{data.projId}</p>
                  </div>
                  <div className="infoOrderPatients infoOrderElement">
                     <h2>Pacjenci:</h2>
                     <div className='infoOrderPatientsDiv'>
                        {DataPatients.map((patData) =>
                           patData.orderId === data.id &&
                           <div className="patientInOrder" key={"key_1_"+patData.id}>
                              {patData.nameS} {patData.surname}
                              <div className='gofar'>
                                 {DataTests.map((testData) =>
                                    testData.patId === patData.id && data.id === testData.orderId &&
                                    <p key={"key_2_"+testData.id}>
                                       {testData.testName}
                                    </p>
                                 )}
                              </div>
                           </div>
                        )}
                     </div>
                  </div>
               </div>
            )}
            <div className="buttons">
               <div className="delete-ord-info" onClick={() => deleteOrder()}>Wycofaj</div>
               <Link to={"/orders/orderInfoEdit?ord_id="+order_id} className='edit-ord-info'>Edytuj</Link>
               <Link to="/orders" className='close-ord-info'>Wróć</Link>
            </div>
         </div>
      </div>
    </div>
  )
}

export default OrderInfo
