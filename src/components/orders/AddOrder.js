import React from 'react'
import { Link } from "react-router-dom";


function AddOrder() {
   const checkIfCorrect = () => {
      let correct = true
      const nameCheck = document.getElementById("nameCheck");

      if(!nameCheck.value){
         nameCheck.classList.add('error')
         correct = false
      }
      
      if(nameCheck.value !== ""){
         nameCheck.classList.remove('error')
      }

      if(correct){
         console.log("Add to database:\n\tOrder name: "+nameCheck.value)
         window.location.href = "/orders";
      }
   }

  return (
    <div>
      <div className="box">
         <div className="main">
            <h1>Dodaj zamówienie</h1>
            <form action='###'>
               <div className="entering">
                  <label htmlFor="orderName"><r>*</r>Nazwa</label>
                  <input id ="nameCheck" type="text" name='orderName' placeholder='Wprowadź nazwę zamówienia' required/>
               </div>
               <div className="buttons">
                  <Link to="/orders" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Dodaj" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default AddOrder
