import React from 'react'
import { DataPatients } from '../../ex_data/DataPatients'
import { DataTests } from '../../ex_data/DataTests'

function ShowEditOrderPatients(gottenProjId) {
   const orderIdFromUrl = gottenProjId.orderID
   return (
      <>
      {DataTests.map((data) =>
         data.orderId === parseInt(orderIdFromUrl) &&
            <div id={"id_"+data.patId+"_"+data.id} className="in-project-patient" key={"key_"+data.patId+"_"+data.id}>
               <div className="del" onClick={() => document.getElementById("id_"+data.patId+"_"+data.id).remove()}>
                  <p>X</p>
               </div>
               {DataPatients.map((patData) => 
                  patData.id === data.patId &&
                  <div className="patChosenName" key="key">
                     <input type="text" className="chosen-patient" disabled value={patData.nameS+", "+patData.surname+", "+patData.pesel+", "+patData.city+", "+patData.address}/>
                     <p>{patData.nameS} {patData.surname}, {patData.pesel}, {patData.city}, {patData.address}</p>
                  </div>
               )}
               <div className='selectTest'>
                  <select className="orderInfoEditSelect chooseTest" name="test" id="test" required>
                     {DataTests.map((testData) =>
                        <option value={testData.id} key={"key_"+testData.id}>{testData.testName}</option>
                     )}
                  </select>
               </div>
            </div>
      )}
      </>
   )
}

export default ShowEditOrderPatients
