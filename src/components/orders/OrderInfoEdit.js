import React from 'react'
import { Link } from "react-router-dom";
import { DataOrders } from '../../ex_data/DataOrders';
import { DataPatients } from '../../ex_data/DataPatients';
import { DataProjects } from '../../ex_data/DataProjects';
import ShowEditOrderPatients from './ShowEditOrderPatients';


function OrderInfoEdit() {
   const queryString = window.location.search;
   const urlParam = new URLSearchParams(queryString);
   const order_id = urlParam.get('ord_id')

  return (
    <div>
      <div className="box infoOrderEditBox">
         <div>
            {DataOrders.map((data) => 
               data.id === parseInt(order_id) &&
               <form action='###' key="key">
                  <div className="entering">
                  <label htmlFor="name">Nazwa zlecenia</label>
                     <input id ="nameCheck" type="text" name='name' placeholder="Wprowadź nazwę zlecenia" defaultValue={data.orderName} required/>
                  </div>
                  <div className="entering">
                     <label htmlFor="id">Id zamówienia</label>
                     <input id ="idCheck" type="text" name='id' disabled required defaultValue={data.id}/>
                  </div>
                  <div className="entering">
                     <label htmlFor="done">Zrealizowano</label>
                     <select className="orderInfoEditSelect" id="doneCheck" type="text" name='done' required defaultValue={data.done ? "yes": "no"}>
                        <option value="no">NIE</option>
                        <option value="yes">TAK</option>
                     </select>
                  </div>
                  <div className="entering">
                     <label htmlFor="date">Data utworzenia</label>
                     <input id ="dateCheck" type="date" name='date' required defaultValue={data.date}/>
                  </div>
                  <div className="entering">
                     <label htmlFor="project">Projekt</label>
                     <select className="orderInfoEditSelect" id ="projectCheck" name='project' defaultValue={data.projId}>
                        {DataProjects.map((projData) => 
                           <option value={projData.id} key={"key_"+projData.id}>{projData.projName}</option>
                        )}
                     </select>
                  </div>
                  <div className="entering dataEditing">
                     {<ShowEditOrderPatients orderID={order_id} />}
                  </div>
                  <select name='patName' id ="patNameCheck" className='edit-pat-in-proj' defaultValue="">
                     <option value="" disabled>Wybierz pacjenta</option>
                     {DataPatients.map((data) =>
                        data.agreed === "checked" &&
                        <option value={"patId_"+data.id} key={"key_"+data.id} className={data.agreed}>
                           {data.nameS} {data.surname}, {data.pesel}, {data.city} {data.address}
                        </option>
                     )}      
                  </select>
                     <div className="addOption">
                        <h2>Dodaj</h2>
                     </div>
                  </form>
                  
               )}
                  <div className="buttons">
                     <Link to={"/orders/orderInfo?ord_id="+order_id} className='delete-ord-info'>Anuluj</Link>
                     <input type="button" id="save-button" className="edit-ord-info" value="Zapisz"/>
                  </div>
         </div>
      </div>
    </div>
  )
}

export default OrderInfoEdit
