import React, { useState } from 'react'
import { DataPatients } from "../../ex_data/DataPatients"

function DotsPat({ setPage }) {
  const maxId = DataPatients.length
  const maxPage = Math.ceil(maxId / 10)
  const [pageInt, setPageInt] = useState(1)
  const pagesList = []

  for(let index = 1; index <= maxPage; index++){
    pagesList[index-1] = index
  }

  const setValue = (pg) => {
    setPage(pg)
    setPageInt(pg)
    document.querySelector(".page-dot.current").classList.remove("current")
    document.querySelector("#d_"+pg).classList.add("current")
  }

  return (
    <>
      {pagesList.map((data, index) => 
        index < 1 && 3 < parseInt(pageInt) &&
          <div id={"d_"+data} className="page-dot" key={"key_"+data} onClick={() => setValue(index+1)}>
          {data}
          </div>
      )}
      {pagesList.map((data, index) => 
        index < 1 && 4 < parseInt(pageInt) &&
        <h3 className="dot3" key="h3_1">...</h3>
      )}
      {pagesList.map((data, index) => 
        index >= parseInt(pageInt)-3 && index < parseInt(pageInt)-1 &&
        <div id={"d_"+data} className="page-dot" key={"key_"+data} onClick={() => setValue(index+1)}>
        {data}
        </div>
      )}
      {pagesList.map((data, index) => 
        index === parseInt(pageInt)-1 &&
        <div id={"d_"+data} className="page-dot current" key={"key_"+data} onClick={() => setValue(index+1)}>
        {data}
        </div>
      )}
      {pagesList.map((data, index) => 
        index >= parseInt(pageInt) && index < parseInt(pageInt)+2 &&
        <div id={"d_"+data} className="page-dot" key={"key_"+data} onClick={() => setValue(index+1)}>
        {data}
        </div>
      )}
      {pagesList.map((data, index) => 
        index > parseInt(maxPage)-2 && index > parseInt(pageInt)+2 &&
        <h3 className="dot3" key="h3_2">...</h3>
      )}
      {pagesList.map((data, index) => 
        index > parseInt(maxPage)-2 && index > parseInt(pageInt)+1 &&
          <div id={"d_"+data} className="page-dot" key={"key_"+data} onClick={() => setValue(index+1)}>
          {data}
          </div>
      )}
    </>
  )
}

export default DotsPat
