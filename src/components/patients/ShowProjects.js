import React from 'react'
import { DataProjects } from '../../ex_data/DataProjects'

function ShowProjects(gottenPatientId) {
   const chosenPatient = gottenPatientId.projId
   return (
      <>
      {DataProjects.map((data) =>
         data.patId === parseInt(chosenPatient) &&
         <tr id={"patId_"+data.patId} className="details" key={"key_"+data.id}>
            <td>
               {data.projName}
            </td>
         </tr>
      )}
      </>
   )
}

export default ShowProjects
