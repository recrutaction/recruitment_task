import React from 'react'
import { Link } from "react-router-dom"
import PatientList from "./PatientList"

function Patients() {

  return (
    <div className='patients-tab'>
      <div className="top">
         <div className="search">
            <input type="text" placeholder="Wyszukaj pacjenta"/>
         </div>
         <div className="add">
            <Link to="/patients/addPatient" className='add-patient'>Dodaj pacjenta</Link>
         </div>
      </div>
         <PatientList /> 
    </div>
  )
}

export default Patients
