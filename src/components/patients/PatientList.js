import React from 'react'
import { DataPatients } from "../../ex_data/DataPatients";
import { Link } from "react-router-dom";
import DotsPat from './DotsPat'
import { useState } from 'react'
import arrow from '../../img/arrow.png'
import ShowProjects from './ShowProjects'

function PatientList() {
   const [chosenUser, setChosenUser] = useState({
      id: "",
      nameS: "",
      surname: "",
      pesel: "",
      city: "",
      street: "",
      building: "",
      apartment: ""
   })

   const checkIfChar = (data) => {
      if(data.includes("/"))
         return data.split(",").pop().split("/").pop()
      else
      return "-"
   }
   const showDetails = (patId) => {
      const idOfPat = patId.split("_").pop()
      DataPatients.map((data, index) =>
         index === parseInt(idOfPat)-1 &&
         setChosenUser({
            id: idOfPat,
            nameS: data.nameS,
            surname: data.surname,
            pesel: data.pesel,
            city: data.city,
            street: data.address.split(",").shift(),
            building: data.address.split(",").pop().split("/").shift(),
            apartment: checkIfChar(data.address)
         })
      )
      document.querySelector('.patient-details').style.display = "flex"
   }

   var sortedAZ = true
   var currentSorted = "patId"
   const sortPat = (sortBy) => {
      const idArrow1 = document.getElementById("arrow-for-id-1")
      const idArrow2 = document.getElementById("arrow-for-id-2")
      const nameArrow1 = document.getElementById("arrow-for-name-1")
      const nameArrow2 = document.getElementById("arrow-for-name-2")
      const surnameArrow1 = document.getElementById("arrow-for-surname-1")
      const surnameArrow2 = document.getElementById("arrow-for-surname-2")
      const peselArrow1 = document.getElementById("arrow-for-pesel-1")
      const peselArrow2 = document.getElementById("arrow-for-pesel-2")
      const addressArrow1 = document.getElementById("arrow-for-address-1")
      const addressArrow2 = document.getElementById("arrow-for-address-2")
      const arrowCurrent1 = document.querySelector(".active-1")
      const arrowCurrent2 = document.querySelector(".active-2")

      switch (sortBy) {
         case "patId":
            if(currentSorted !== "patId"){
               sortedAZ = true
               currentSorted = "patId"
               console.log("Column ID sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               idArrow1.classList.add("active-1")
               idArrow2.classList.add("active-2")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else if(currentSorted === "patId" && !sortedAZ){
               sortedAZ = true
               currentSorted = "patId"
               console.log("Column ID sorted ascendingly-2")
               
               idArrow1.classList.remove("desc")
               idArrow2.classList.remove("desc")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "patId"
               console.log("Column ID sorted descendingly-3")

               idArrow1.classList.remove("asc")
               idArrow2.classList.remove("asc")
               idArrow2.classList.add("desc")
               idArrow1.classList.add("desc")
            }
            break;
         case "patNameS":
            if(currentSorted !== "patNameS"){
               sortedAZ = true
               currentSorted = "patNameS"
               console.log("Column name/names sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               nameArrow1.classList.add("active-1")
               nameArrow2.classList.add("active-2")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else if(currentSorted === "patNameS" && !sortedAZ){
               sortedAZ = true
               currentSorted = "patNameS"
               console.log("Column name/names sorted ascendingly-2")
               
               nameArrow1.classList.remove("desc")
               nameArrow2.classList.remove("desc")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "patNameS"
               console.log("Column name/names sorted descendingly-3")

               nameArrow1.classList.remove("asc")
               nameArrow2.classList.remove("asc")
               nameArrow1.classList.add("desc")
               nameArrow2.classList.add("desc")
            }
            break;
         case "patSurname":
            if(currentSorted !== "patSurname"){
               sortedAZ = true
               currentSorted = "patSurname"
               console.log("Column surname sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               surnameArrow1.classList.add("active-1")
               surnameArrow2.classList.add("active-2")
               surnameArrow1.classList.add("asc")
               surnameArrow2.classList.add("asc")
            }
            else if(currentSorted === "patSurname" && !sortedAZ){
               sortedAZ = true
               currentSorted = "patSurname"
               console.log("Column surname sorted ascendingly-2")
               
               surnameArrow1.classList.remove("desc")
               surnameArrow2.classList.remove("desc")
               surnameArrow1.classList.add("asc")
               surnameArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "patSurname"
               console.log("Column surname sorted descendingly-3")

               surnameArrow1.classList.remove("asc")
               surnameArrow2.classList.remove("asc")
               surnameArrow1.classList.add("desc")
               surnameArrow2.classList.add("desc")
            }
            break;
         case "patPesel":
            if(currentSorted !== "patPesel"){
               sortedAZ = true
               currentSorted = "patPesel"
               console.log("Column pesel sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               peselArrow1.classList.add("active-1")
               peselArrow2.classList.add("active-2")
               peselArrow1.classList.add("asc")
               peselArrow2.classList.add("asc")
            }
            else if(currentSorted === "patPesel" && !sortedAZ){
               sortedAZ = true
               currentSorted = "patPesel"
               console.log("Column pesel sorted ascendingly-2")
               
               peselArrow1.classList.remove("desc")
               peselArrow2.classList.remove("desc")
               peselArrow1.classList.add("asc")
               peselArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "patPesel"
               console.log("Column pesel sorted descendingly-3")

               peselArrow1.classList.remove("asc")
               peselArrow2.classList.remove("asc")
               peselArrow1.classList.add("desc")
               peselArrow2.classList.add("desc")
            }
            break;
         case "patAddress":
            if(currentSorted !== "patAddress"){
               sortedAZ = true
               currentSorted = "patAddress"
               console.log("Column address sorted ascendingly-1")

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               addressArrow1.classList.add("active-1")
               addressArrow2.classList.add("active-2")
               addressArrow1.classList.add("asc")
               addressArrow2.classList.add("asc")
            }
            else if(currentSorted === "patAddress" && !sortedAZ){
               sortedAZ = true
               currentSorted = "patAddress"
               console.log("Column address sorted ascendingly-2")
               
               addressArrow1.classList.remove("desc")
               addressArrow2.classList.remove("desc")
               addressArrow1.classList.add("asc")
               addressArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "patAddress"
               console.log("Column address sorted descendingly-3")

               addressArrow1.classList.remove("asc")
               addressArrow2.classList.remove("asc")
               addressArrow1.classList.add("desc")
               addressArrow2.classList.add("desc")
            }
            break;
         default:
            console.log("ERROR")
            break;
      }
      
   }

   const hideExtra = () => document.querySelector('.patient-details').style.display = "none"
   const [page, setPage] = useState(1);
   const minPageId = (page - 1) * 10
   const maxPageId = page * 10

   
  return (
    <>
      <div className="patient-details">
         <div className="info">
            <h1>{chosenUser.nameS} {chosenUser.surname}</h1>
            <div className="line">
               <h2>Id</h2>
               <p>{chosenUser.id}</p>
            </div>
            <div className="line">
               <h2>Pesel</h2>
               <p>{chosenUser.pesel}</p>
            </div>
            <div className="line">
               <h2>Miasto</h2>
               <p>{chosenUser.city}</p>
            </div>
            <div className="line">
               <h2>Ulica</h2>
               <p>{chosenUser.street}</p>
            </div>
            <div className="line">
               <h2>Budynek</h2>
               <p>{chosenUser.building}</p>
            </div>
            <div className="line">
               <h2>Mieszkanie</h2>
               <p>{chosenUser.apartment}</p>
            </div>
         </div>
         <div className="proj">
            <h1>Projekty</h1>
            <div className="overflow">
               <table className="detail-info">
                  <thead>
                     <tr>
                        <th>Nazwa projektu</th>
                     </tr>
                  </thead>
                  <tbody>
                     {<ShowProjects projId={chosenUser.id} />}
                  </tbody>
               </table>
            </div>
         </div>
         <div className="settings">
            <div className="edit-delete">
               <div className="delete">
                  <p>Usuń</p>
               </div>
               <Link to={"/patients/editPatient?id="+chosenUser.id} className="edit">
                  <p>Edytuj</p>
               </Link>
            </div>
            <div className="go-back">
               <div className="close" onClick={() => hideExtra()}>
                  <p>Zamknij</p>
               </div>
            </div>
         </div>
      </div>
      <table className="patient-list">
         <tbody>
            <tr>
               <th onClick={() => sortPat("patId")} id='pat-id' className='id'>
                  <div id='sort-id' className="sorting">
                     <img alt="sort_arrow" src={arrow} id="arrow-for-id-1" className='sorted asc active-1'/>
                     <h2>Id</h2>
                     <img alt="sort_arrow" src={arrow} id="arrow-for-id-2" className='sorted asc active-2'/>
                  </div>
               </th>
               <th onClick={() => sortPat("patNameS")} id='name-s' className='name-s'>
                  <div id='sort-name-s' className="sorting">
                     <img alt="sort_arrow" src={arrow} id="arrow-for-name-1"className='sorted'/>
                     <h2>Imię/Imiona</h2>
                     <img alt="sort_arrow" src={arrow} id="arrow-for-name-2"className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortPat("patSurname")} id='surname' className='surname'>
                  <div id='sort-surname' className="sorting">
                     <img alt="sort_arrow" src={arrow} id="arrow-for-surname-1" className='sorted'/>
                     <h2>Nazwisko</h2>
                     <img alt="sort_arrow" src={arrow} id="arrow-for-surname-2" className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortPat("patPesel")} id='pesel' className='pesel'>
                  <div id='sort-pesel' className="sorting">
                     <img alt="sort_arrow" src={arrow} id="arrow-for-pesel-1" className='sorted'/>
                     <h2>Pesel</h2>
                     <img alt="sort_arrow" src={arrow} id="arrow-for-pesel-2" className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortPat("patAddress")} id='address' className='address'>
                  <div id='sort-address' className="sorting">
                     <img alt="sort_arrow" src={arrow} id="arrow-for-address-1" className='sorted '/>
                     <h2>Adres</h2>
                     <img alt="sort_arrow" src={arrow} id="arrow-for-address-2" className='sorted'/>
                  </div>
               </th>
               <th className='manage'></th>
            </tr>
         {DataPatients.map((data, index) =>
         index >= minPageId && index < maxPageId &&
            <tr key={"pat_id_" + data.id}>
               <td className="id">{data.id}</td>
               <td className="nameS">{data.nameS}</td>
               <td className="surname">{data.surname}</td>
               <td className="pesel">{data.pesel}</td>
               <td className="address">{data.city}, {data.address}</td>
               <td className="info">
                  <div className="more" onClick={() => showDetails("pat_id_" + data.id)}>
                     <p>Więcej</p>
                  </div>
               </td> 
            </tr>
         )}
         </tbody>
      </table>
      <div className="bot">
            <DotsPat setPage={setPage} />
      </div>
         
    </>
  )
}

export default PatientList
