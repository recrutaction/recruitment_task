import React from 'react'
import { Link } from "react-router-dom";


function AddPatient() {
   const checkIfCorrect = () => {
      let correct = true
      const nameCheck = document.getElementById("nameCheck");
      const secondNameCheck = document.getElementById("secondNameCheck");
      const surnameCheck = document.getElementById("surnameCheck");
      const peselCheck = document.getElementById("peselCheck");
      const cityCheck = document.getElementById("cityCheck");
      const streetCheck = document.getElementById("streetCheck");
      const buildCheck = document.getElementById("buildCheck");
      const apartmentCheck = document.getElementById("apartmentCheck");
      var sName = "-"
      var address = cityCheck.value+", "+streetCheck.value+" "+buildCheck.value

      if(!nameCheck.value){
         nameCheck.classList.add('error')
         correct = false
      }
      if(!surnameCheck.value){
         surnameCheck.classList.add('error')
         correct = false
      }
      if(!peselCheck.value){
         peselCheck.classList.add('error')
         correct = false
      }
      if(!cityCheck.value){
         cityCheck.classList.add('error')
         correct = false
      }
      if(!streetCheck.value){
         streetCheck.classList.add('error')
         correct = false
      }
      if(!buildCheck.value){
         buildCheck.classList.add('error')
         correct = false
      }
      
      if(nameCheck.value !== ""){
         nameCheck.classList.remove('error')
      }
      if(surnameCheck.value !== ""){
         surnameCheck.classList.remove('error')
      }
      if(peselCheck.value !== ""){
         const isnum = /^\d+$/.test(peselCheck.value);
         if(!isnum){
            peselCheck.classList.add('error')
            correct = false
         }
         else
            peselCheck.classList.remove('error')
      }
      if(cityCheck.value !== ""){
         cityCheck.classList.remove('error')
      }
      if(streetCheck.value !== ""){
         streetCheck.classList.remove('error')
      }
      if(buildCheck.value !== ""){
         buildCheck.classList.remove('error')
      }

      if(secondNameCheck.value)
         sName = secondNameCheck.value
      if(apartmentCheck.value)
         address += "/"+apartmentCheck.value

      if(correct){
         console("Add to database:\n\tFirst name: "+nameCheck.value+"\n\tSecond name: "+sName+"\n\tSurname: "+surnameCheck.value+"\n\tPesel: "+peselCheck.value+"\n\tAddress: "+address)
         window.location.href = "/patients";
      }
   }

  return (
    <div>
      <div className="box">
         <div className="main patientMain">
            <h1>Dodaj pacjenta</h1>
            <form action='###'>
               <div className="entering">
                  <label htmlFor="firstName"><r>*</r>Imię</label>
                  <input id ="nameCheck" type="text" name='firstName' placeholder='Wprowadź imię' required/>
               </div>
               <div className="entering">
                  <label htmlFor="secondName">Drugie imię</label>
                  <input id="secondNameCheck" type="text" name='secondName' placeholder='Wprowadź drugie imię (opcjonalnie)'/>
               </div>
               <div className="entering">
                  <label htmlFor="surname"><r>*</r>Nazwisko</label>
                  <input id ="surnameCheck" type="text" name='surname' placeholder='Wprowadź nazwisko' required/>
               </div>
               <div className="entering">
                  <label htmlFor="pesel"><r>*</r>Pesel</label>
                  <input id ="peselCheck" type="text" name='pesel' placeholder='Wprowadź pesel' required/>
               </div>
               <div className="entering">
                  <label htmlFor="city"><r>*</r>Miasto</label>
                  <input id ="cityCheck" type="text" name='city' placeholder='Wprowadź miasto' required/>
               </div>
               <div className="entering">
                  <label htmlFor="street"><r>*</r>Ulica</label>
                  <input id ="streetCheck" type="text" name='street' placeholder='Wprowadź ulicę' required/>
               </div>
               <div className="entering">
                  <label htmlFor="building"><r>*</r>Budynek</label>
                  <input id ="buildCheck" type="text" name='building' placeholder='Wprowadź numer budynku' required/>
               </div>
               <div className="entering">
                  <label htmlFor="apartment">Mieszkanie</label>
                  <input id="apartmentCheck" type="text" name='apartment' placeholder='Wprowadź numer mieszkania (opcjonalnie)'/>
               </div>
               <div className="buttons">
                  <Link to="/patients" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Dodaj" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default AddPatient
