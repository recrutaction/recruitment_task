import React from 'react'
import TreatmentList from './TreatmentList'
import { useState } from "react"
import TestTypeChart from "./TestTypeChart"
import { DataTests } from "../../ex_data/DataTests"
import { DataProjects } from "../../ex_data/DataProjects"
import { DataPatients } from "../../ex_data/DataPatients"

function Summary() {
  const idProjList = DataProjects.map((data) => data.id)
  const resNumList = DataTests.map((data) => data.executed)
  var sum = 0;
  resNumList.forEach(e => {
     sum += e
  });
  const idPatList = DataPatients.map((data) => data.id)
  const [researches] = useState({
      labels: DataTests.map((data) => data.testName),
      datasets: [
        {
        label: "Researches",
        data: DataTests.map((data) => data.executed),
        backgroundColor: [
           "#EDF2F4",
           "#7C83C6",
        ],
        borderColor: "#575D90",
        borderWidth: 2,
        },
      ],
      options: {
         responsive: true,
         maintainAspectRatio: false,
      },
  })

  return (
    <div className='summary-tab'>
      <div className='reg-patients card'>
        <h2>Zarejestrowano</h2>
        <h1>{idPatList.length}</h1>
        <h2>Pacjentów</h2>
      </div>
      <div className='reg-projects card'>
        <h2>Istnieje</h2>
        <h1>{idProjList.length}</h1>
        <h2>Projektów</h2>
      </div>
      <div className='patients-per-proj card'>
        <h2>Wykonano</h2>
        <h1>{sum}</h1>
        <h2>Badań</h2>
      </div>
      <div className='researches card'>
        <h1>Badania</h1>
        <div className="res-content">
          <TreatmentList />
          <TestTypeChart chartData={researches} />
        </div>
      </div>
    </div>
  )
}

export default Summary
