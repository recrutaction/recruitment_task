import React from "react";
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS } from "chart.js/auto";

function TestTypeChart({ chartData }) {
   return (
      <div className="chart" style={{height:"60vh", width:"60vh"}}>
         <Pie data={chartData} options={{responsive: true, maintainAspectRatio: false}} />
      </div>
   )
}

export default TestTypeChart;