import React from 'react'
import { DataTests } from "../../ex_data/DataTests";

function TreatmentList() {
   return (
      <div className="list">
         <div className="list-header">
            <h2 className="h-name">Nazwa badania</h2>
            <h2 className="b-name">Wykonano razy</h2>
         </div>
         <div className="list-body">
            <div className="name">
               {DataTests.map((data) =>
                     <p key={"key_name_"+data.id}>{data.testName}</p>
               )}
            </div>
            <div className="number">
               {DataTests.map((data) =>
                     <p key={"key_name_"+data.id}>{data.executed}</p>
               )}
            </div>
         </div>
      </div>
   )
}

export default TreatmentList
