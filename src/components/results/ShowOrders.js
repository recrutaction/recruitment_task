import React from 'react'
import { DataOrders } from '../../ex_data/DataOrders'
import { Link } from "react-router-dom";

function ShowOrders(gottenTestId) {
   const chosenTest = gottenTestId.testId


   return (
      <>
      {DataOrders.map((data) =>
         data.testId === parseInt(chosenTest) &&
         <tr id={"ordId_"+data.testId} className="details" key={"key_"+data.id}>
            <td>
               {data.orderName}
            </td>
            <td className='small'>
               <Link to={"/orders/orderInfo?ord_id=" + data.id} className="edit-res">
                  <p>Zobacz</p>
               </Link>
            </td>
         </tr>
      )}
      </>
   )
}

export default ShowOrders
