import React from 'react'
import { DataTests } from "../../ex_data/DataTests";
import { Link } from "react-router-dom";
import DotsTest from './DotsTest'
import { useState } from 'react'
import arrow from '../../img/arrow.png'
import ShowOrders from './ShowOrders'

function TestList() {
   const [ChosenTest, setChosenTest] = useState({
      id: "",
      testName: "",
      numbOfPat: "",
   })
 
   const showDetails = (testId) => {
      const idOfTest = testId.split("_").pop()
      DataTests.map((data, index) =>
         index === parseInt(idOfTest)-1 &&
         setChosenTest({
            id: idOfTest,
            testName: data.testName,
            numbOfPat: data.numbOfPat
         })
      )
      document.querySelector('.test-details').style.display = "flex"
   }

   var sortedAZ = true
   var currentSorted = "testId"
   const sortTest = (sortBy) => {
      const idArrow1 = document.getElementById("arrow-for-id-1")
      const idArrow2 = document.getElementById("arrow-for-id-2")
      const nameArrow1 = document.getElementById("arrow-for-test-name-1")
      const nameArrow2 = document.getElementById("arrow-for-test-name-2")
      const testResultArrow1 = document.getElementById("arrow-for-test-result-1")
      const testResultArrow2 = document.getElementById("arrow-for-test-result-2")
      const arrowCurrent1 = document.querySelector(".active-1")
      const arrowCurrent2 = document.querySelector(".active-2")

      switch (sortBy) {
         case "testId":
            if(currentSorted !== "testId"){
               sortedAZ = true
               currentSorted = "testId"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               idArrow1.classList.add("active-1")
               idArrow2.classList.add("active-2")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else if(currentSorted === "testId" && !sortedAZ){
               sortedAZ = true
               currentSorted = "testId"
               
               idArrow1.classList.remove("desc")
               idArrow2.classList.remove("desc")
               idArrow1.classList.add("asc")
               idArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "testId"

               idArrow1.classList.remove("asc")
               idArrow2.classList.remove("asc")
               idArrow2.classList.add("desc")
               idArrow1.classList.add("desc")
            }
            break;
         case "testName":
            if(currentSorted !== "testName"){
               sortedAZ = true
               currentSorted = "testName"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               nameArrow1.classList.add("active-1")
               nameArrow2.classList.add("active-2")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else if(currentSorted === "testName" && !sortedAZ){
               sortedAZ = true
               currentSorted = "testName"
               
               nameArrow1.classList.remove("desc")
               nameArrow2.classList.remove("desc")
               nameArrow1.classList.add("asc")
               nameArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "testName"

               nameArrow1.classList.remove("asc")
               nameArrow2.classList.remove("asc")
               nameArrow1.classList.add("desc")
               nameArrow2.classList.add("desc")
            }
            break;
         case "testResult":
            if(currentSorted !== "testResult"){
               sortedAZ = true
               currentSorted = "testResult"

               arrowCurrent1.classList.remove("asc")
               arrowCurrent2.classList.remove("asc")
               arrowCurrent1.classList.remove("desc")
               arrowCurrent2.classList.remove("desc")
               arrowCurrent1.classList.remove("active-1")
               arrowCurrent2.classList.remove("active-2")
               testResultArrow1.classList.add("active-1")
               testResultArrow2.classList.add("active-2")
               testResultArrow1.classList.add("asc")
               testResultArrow2.classList.add("asc")
            }
            else if(currentSorted === "testResult" && !sortedAZ){
               sortedAZ = true
               currentSorted = "testResult"
               
               testResultArrow1.classList.remove("desc")
               testResultArrow2.classList.remove("desc")
               testResultArrow1.classList.add("asc")
               testResultArrow2.classList.add("asc")
            }
            else{
               sortedAZ = false
               currentSorted = "testResult"

               testResultArrow1.classList.remove("asc")
               testResultArrow2.classList.remove("asc")
               testResultArrow1.classList.add("desc")
               testResultArrow2.classList.add("desc")
            }
            break;
         default:
            console.log("ERROR")
            break;
      }
      
   }

   const hideExtra = () => document.querySelector('.test-details').style.display = "none"
   const [page, setPage] = useState(1);
   const minPageId = (page - 1) * 10
   const maxPageId = page * 10

   
  return (
    <>
      <div className="test-details">
         <div className="info-test">
            <h1>{ChosenTest.testName}</h1>
            <div className="test">
               <div className="overflow">
                  <table className="detail-info">
                     <thead>
                        <tr>
                           <th>Zamówienie</th>
                        </tr>
                     </thead>
                     <tbody>
                        <ShowOrders testId={ChosenTest.id}/>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div className="settings">
            <div className="edit-delete">
               <div className="delete">
                  <p>Usuń</p>
               </div>
               <Link to={"/results/editTest?"+ChosenTest.id} className="edit">
                  <p>Edytuj</p>
               </Link>
            </div>
            <div className="go-back">
               <div className="close" onClick={() => hideExtra()}>
                  <p>Zamknij</p>
               </div>
            </div>
         </div>
      </div>
      <table className="test-list">
         <tbody>
            <tr>
               <th onClick={() => sortTest("testId")} id='test-id' className='id'>
                  <div id='sort-id' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-1" className='sorted asc active-1'/>
                     <h2>Id</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-id-2" className='sorted asc active-2'/>
                  </div>
               </th>
               <th onClick={() => sortTest("testName")} id='test-name' className='test-name'>
                  <div id='sort-test-name' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-test-name-1"className='sorted'/>
                     <h2>Nazwa Testektu</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-test-name-2"className='sorted'/>
                  </div>
               </th>
               <th onClick={() => sortTest("testResult")} id='test-result' className='test-result'>
                  <div id='sort-test-result' className="sorting">
                     <img alt="sort-arrow" src={arrow} id="arrow-for-test-result-1"className='sorted'/>
                     <h2>Wynik</h2>
                     <img alt="sort-arrow" src={arrow} id="arrow-for-test-result-2"className='sorted'/>
                  </div>
               </th>
               <th className='manage'></th>
            </tr>
         {DataTests.map((data, index) =>
         index >= minPageId && index < maxPageId &&
            <tr key={"test_id_" + data.id}>
               <td className="id">{data.id}</td>
               <td className="testName">{data.testName}</td>
               <td className="testResult">{data.result}</td>
               <td className="info">
                  <div className="more" onClick={() => showDetails("test_id_" + data.id)}>
                     <p>Więcej</p>
                  </div>
               </td> 
            </tr>
         )}
         </tbody>
      </table>
      <div className="bot">
            <DotsTest setPage={setPage} />
      </div>
         
    </>
  )
}

export default TestList
