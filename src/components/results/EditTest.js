import React from 'react'
import { Link } from "react-router-dom";


function EditTest() {
   const checkIfCorrect = () => {
      let correct = true
      const nameCheck = document.getElementById("nameCheck");
      const resultCheck = document.getElementById("resultCheck");

      if(!nameCheck.value){
         nameCheck.classList.add('error')
         correct = false
      }
      
      if(nameCheck.value !== ""){
         nameCheck.classList.remove('error')
      }

      if(!resultCheck.value){
         resultCheck.value = "-"
      }

      if(correct){
         console.log("Add to database:\n\tTest name: "+nameCheck.value)
         window.location.href = "/results";
      }
   }

  return (
    <div>
      <div className="box">
         <div className="main">
            <h1>Edytuj badanie</h1>
            <form action='###'>
               <div className="entering">
                  <label htmlFor="projName"><r>*</r>Nazwa</label>
                  <input id ="nameCheck" type="text" name='projName' placeholder='Wprowadź nazwę badania' required/>
               </div>
               <div className="entering">
                  <label htmlFor="projName">Wynik</label>
                  <input id ="resultCheck" type="text" name='projName' placeholder='Wprowadź wynik' required/>
               </div>               
               <div className="buttons">
                  <Link to="/results" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Zapisz" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default EditTest
