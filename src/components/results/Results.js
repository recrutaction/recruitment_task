import React from 'react'
import { Link } from "react-router-dom"
import TestsList from "./TestsList"

function Results() {

  return (
    <div className='results-tab'>
      <div className="top">
         <div className="search">
            <input type="text" placeholder="Wyszukaj badanie"/>
         </div>
         <div className="add">
            <Link to="/results/addTest" className='add-test'>Dodaj badanie</Link>
         </div>
      </div>
         <TestsList /> 
    </div>
  )
}

export default Results
