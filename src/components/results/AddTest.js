import React from 'react'
import { Link } from "react-router-dom";


function AddTest() {
   const checkIfCorrect = () => {
      let correct = true
      const nameCheck = document.getElementById("nameCheck");

      if(!nameCheck.value){
         nameCheck.classList.add('error')
         correct = false
      }
      
      if(nameCheck.value !== ""){
         nameCheck.classList.remove('error')
      }

      if(correct){
         console.log("Add to database:\n\tTest name: "+nameCheck.value)
         window.location.href = "/results";
      }
   }

  return (
    <div>
      <div className="box">
         <div className="main">
            <h1>Dodaj badanie</h1>
            <form action='###'>
               <div className="entering">
                  <label htmlFor="testName"><r>*</r>Nazwa</label>
                  <input id ="nameCheck" type="text" name='testName' placeholder='Wprowadź nazwę badania' required/>
               </div>
               <div className="buttons">
                  <Link to="/results" className='cancel'>Anuluj</Link>
                  <input type="button" name="addPat" id="addPat" value="Utwórz" onClick={() => checkIfCorrect()}/>
               </div>
            </form>
         </div>
      </div>
    </div>
  )
}

export default AddTest
