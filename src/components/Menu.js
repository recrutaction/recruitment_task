import { Link } from "react-router-dom";
import React from 'react';

const Menu = () => {

   const [activeTab, setActiveTab] = React.useState('sum')
   const changeTabToSum = () => setActiveTab('sum')
   const changeTabToPat = () => setActiveTab('pat')
   const changeTabToPro = () => setActiveTab('pro')
   const changeTabToOrd = () => setActiveTab('ord')
   const changeTabToRes = () => setActiveTab('res')

   return (
      <div className="menu">
         <div className="name">
            <h1>Panel Zarządzania</h1>
         </div>
         <div className="tabs">
               <Link to="/" onClick={changeTabToSum} className={activeTab === "sum" ? "chosen": "not-chosen"}><p>Podsumowanie</p></Link>
               <Link to="/patients" onClick={changeTabToPat} className={activeTab === "pat" ? "chosen": "not-chosen"}><p>Pacjenci</p></Link>
               <Link to="/projects" onClick={changeTabToPro} className={activeTab === "pro" ? "chosen": "not-chosen"}><p>Projekty</p></Link>
               <Link to="/orders" onClick={changeTabToOrd} className={activeTab === "ord" ? "chosen": "not-chosen"}><p>Zlecenia</p></Link>
               <Link to="/results" onClick={changeTabToRes} className={activeTab === "res" ? "chosen": "not-chosen"}><p>Wyniki</p></Link>
         </div>
      </div>
   )
}
export default Menu
